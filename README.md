# Pastpartu Cluster

```bash

CA_CRT=/workspace/internal/cherubits/certificates/cherubits.hu.pem

openssl x509 -req -in openldap.cherubits.hu.csr -CA $CA_ -CAkey /workspace/internal/cherubits/certificates/cherubits.hu.key -CAcreateserial -out openldap.cherubits.hu.crt -days 825 -sha256 -extfile openldap.cherubits.hu.ext
```

