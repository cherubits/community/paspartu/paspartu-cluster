#!/bin/bash

bash ./generate.sh openldap-client.cherubits.hu ./phpldapadmin/certs/ldap-client
bash ./generate.sh phpldapadmin.cherubits.hu ./phpldapadmin/certs/phpldapadmin
bash ./generate.sh openldap.cherubits.hu ./openldap/certs
