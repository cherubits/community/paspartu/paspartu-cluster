#!/bin/bash

if [ "$#" -ne 2 ]
then
  echo "Usage: Must supply a domain"
  exit 1
fi

CA_CRT=/workspace/internal/cherubits/certificates/cherubits.hu.pem
CA_KEY=/workspace/internal/cherubits/certificates/cherubits.hu.key

DOMAIN=$1
DAYS=825
LENGTH=2048
CERT_DIR=$2

RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[1;33m"
NC="\033[0m"

# Check certificate of certificate-authority
openssl x509 -in $CA_CRT -text -noout

CA_CRT_HASH=$(openssl x509 -noout -modulus -in ${CA_CRT} | openssl md5 | awk -F' ' '{print $2}')
CA_KEY_HASH=$(openssl rsa -noout -modulus -in ${CA_KEY} | openssl md5 | awk -F' ' '{print $2}')

if [ "$CA_CRT_HASH" = "$CA_KEY_HASH" ]; then
  echo "${GREEN}Certificate-authority verified${NC}"
else
  echo "${RED}Wrong certificate-authority${NC}"
fi

cp ${CA_CRT} ${CERT_DIR}/cherubits.hu-ca.crt

openssl genrsa -out $CERT_DIR/$DOMAIN.key ${LENGTH}
# Check key of server certificate
openssl rsa -in $CERT_DIR/$DOMAIN.key -check


openssl req -new \
  -key $CERT_DIR/$DOMAIN.key \
  -out $CERT_DIR/$DOMAIN.csr \
  -subj "/C=HU/ST=Hungary/L=Budapest/O=Cherubits LLC/OU=operations/CN=cherubits.hu"
# Check a CSR
openssl req -text -noout -verify -in $CERT_DIR/$DOMAIN.csr


cat > $CERT_DIR/$DOMAIN.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $DOMAIN
EOF

openssl x509 -req \
    -in $CERT_DIR/$DOMAIN.csr \
    -CA $CA_CRT \
    -CAkey $CA_KEY \
    -CAcreateserial \
    -out $CERT_DIR/$DOMAIN.crt \
    -days 825 \
    -sha256 \
    -extfile $CERT_DIR/$DOMAIN.ext

SERVER_CRT_HASH=$(openssl x509 -noout -modulus -in $CERT_DIR/$DOMAIN.crt | openssl md5 | awk -F' ' '{print $2}')
SERVER_KEY_HASH=$(openssl rsa -noout -modulus -in $CERT_DIR/$DOMAIN.key | openssl md5 | awk -F' ' '{print $2}')


if [ "$SERVER_CRT_HASH" = "$SERVER_KEY_HASH" ]; then
  echo "${GREEN}Server-certificate verified${NC}"
else
  echo "${RED}Wrong server-certificate${NC}"
fi